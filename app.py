import datetime
from flask import Flask, request, jsonify, Markup
from pymongo import MongoClient
import os
from dotenv import load_dotenv

app = Flask(__name__)
load_dotenv()
MONGO_URI = os.environ.get('MONGO_URI')

cluster = MongoClient(MONGO_URI)
print(cluster)
db = cluster['temp_humidity']
col = db['data']

import socket
hostname = socket.gethostname()
IPAddr = socket.gethostbyname(hostname)

print("Your Computer Name is:" + hostname)
print("Your Computer IP Address is:" + IPAddr)
@app.route("/", methods=['GET'])
def about():
    return "Temperature and Humidity Logger"

@app.route("/", methods=['POST'])
def create_entry():
    print('Request Body: {}'.format(request.json))
    temperature = request.json['temp']
    humidity = request.json['humid']
    record_entry = {
        'temperature': temperature,
        'humidity': humidity,
        'formatted_datetime': str(datetime.datetime.now()),
        'datetime': int(datetime.datetime.now().strftime('%s'))
    }
    col.insert_one(record_entry)
    return 'OK'


@app.route("/count", methods=['GET'])
def get_total_records():
    total_records = col.count_documents({})
    print(total_records)
    return {'total': int(total_records)}


@app.route("/latest", methods=['GET'])
def get_latest_record():
    latest_record = col.find().sort([('_id', -1)]).limit(1)

    try:
        current_record = latest_record[0]
        latest_record = {'temperature': current_record['temperature'],
                         'humidity': current_record['humidity'],
                         'datetime': current_record['datetime']
                         }
    except:
        latest_record = {'temperature': 0, 'humidity': 0}
    return latest_record


@app.route("/records", methods=['GET'])
def get_all_records():
    from_date = 0
    to_date = 0
    record_query = {}
    if 'from' in request.args:
        from_date = request.args['from']
        record_query["$gte"] = int(from_date)
    if 'to' in request.args:
        to_date = request.args['to']
        record_query["$lte"] = int(to_date)

    data_records = col.find({"datetime": record_query})
    print("From: {}".format(from_date))
    formatted_records = []
    for record in data_records:
        formatted_records.append({
            'temperature': record['temperature'],
            'humidity': record['humidity'],
            'datetime': record['datetime']
        })
    return {
        'total': len(formatted_records),
        'results': formatted_records
    }

if __name__ == "__main__":
    app.run()
